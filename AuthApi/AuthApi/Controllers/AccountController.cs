﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {

        private readonly ILogger<AccountController> _logger;

        public AccountController(ILogger<AccountController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public ActionResult<LoginResponse> Login(string login, string password)
        {
            var randLogin = new Random();

            if (randLogin.Next(0, 1000) % 5 == 0)
            {
                throw new InvalidOperationException("Logon exception occured");
            }

            if (randLogin.Next(0, 10) % 2 == 0)
            {
                var failResult = new LoginResponse { Code = 1, Message = "Authorization failed" };

                return Ok(failResult);
            }

            var successResult = new LoginResponse { Code = 0, Message = "Authorization succeeded" };

            return Ok(successResult);
        }
    }
}
