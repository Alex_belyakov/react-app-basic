import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Login } from './Login';

ReactDOM.render(
    <Login key="App" />,
  document.getElementById('root')
);
