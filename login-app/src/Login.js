import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';

export class Login extends Component
{
    constructor(props){
        super(props);
        
        this.state = {
            successMessage: false,
            errorMessage: ''         
        };

        this.login = '';
        this.password = '';
    }

    render(){
        return (
            <div className="container">
                <div className="alert alert-danger" role="alert" hidden={!this.state.errorMessage}>{this.state.errorMessage}</div>
                <div className="alert alert-primary" role="alert" hidden={!this.state.successMessage}>{this.state.successMessage}</div>
                
                <div className="mb-3">
                    <h1>Authorization Form</h1>
                    <div className="mb-3">
                            <label className="form-label">Login</label>
                            <input  className="form-control" onChange={this.handleLoginChange.bind(this)}></input>
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Password</label>
                        <input className="form-control" onChange={this.handlePasswordChange.bind(this)}></input>
                    </div>
                </div>
                <div className="mb-3">
                    <button className="btn btn-primary" onClick={this.loginPost.bind(this)}>Login</button>
                </div>
            </div>
        );
    }

    handleLoginChange(event){        
        this.login = event.target.value;
    }

    handlePasswordChange(event){
        this.password = event.target.value;
    }
    
    async loginPost(){
        try{
            this.clearStates();

            var isValid = this.validateForm();
            if (!isValid){
                return;
            }

            var response = await axios.post("/api/account",
                {
                    'login': this.login,
                    'password': this.password
                });        

            if (response.data.code === 0){
                this.setState({
                    successMessage: response.data.message,
                    errorMessage: ''
                });
            }
            else{
                this.setState({
                    successMessage: '',
                    errorMessage: response.data.message
                });
            }
        }
        catch(e){
            console.log(e);
            this.showError("Error occured when logging");
        }
    }

    clearStates(){
        this.setState({
            successMessage: false,
            errorMessage: ''
        });
    }

    validateForm(){
        if (!this.login){
            this.showError("Login is empty");
            
            return false;
        }

        if (!this.password){
            this.showError("Password is empty");
            
            return false;
        }

        return true;
    }

    showError(msg){
        this.setState({
            errorMessage: msg
        });
    }
}